"""
This is the implementation of data preparation for sklearn
"""

import sys
import os
import pandas as pd
import numpy as np

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import logging
from sklearn.model_selection import StratifiedShuffleSplit

# To plot pretty figures
#get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rc('axes', labelsize=14)
mpl.rc('xtick', labelsize=12)
mpl.rc('ytick', labelsize=12)



__author__ = "### Author ###"

logger = XprLogger("explore_data", level=logging.DEBUG)


class ExploreData(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="ExploreData")
        """ Initialize all the required constansts and data her """

    def load_housing_data(self, dataset_path):
        csv_path = os.path.join(dataset_path, "housing.csv")
        return pd.read_csv(csv_path)

    def save_fig(self, fig_id, images_path=None, tight_layout=True, fig_extension="png", resolution=300):
        path = os.path.join(images_path, fig_id + "." + fig_extension)
        logger.debug("Saving figure", fig_id)
        if tight_layout:
            plt.tight_layout()
        plt.savefig(path, format=fig_extension, dpi=resolution)

    def start(self, run_name=None, dataset_path=None, images_path=None):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        if dataset_path is not None and images_path is not None:
            # create dir for images
            if not os.path.isdir(images_path):
                os.makedirs(images_path)

            # load data
            self.send_metrics("loading_data", 0)
            housing = self.load_housing_data(dataset_path)
            self.send_metrics("loading_data", 100)

            # stratified split based on median income
            self.send_metrics("splitting_data", 0)
            housing["income_cat"] = pd.cut(housing["median_income"],
                                       bins=[0., 1.5, 3.0, 4.5, 6., np.inf],
                                       labels=[1, 2, 3, 4, 5])
            split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
            for train_index, test_index in split.split(housing, housing["income_cat"]):
                strat_train_set = housing.loc[train_index]
                strat_test_set = housing.loc[test_index]
            for set_ in (strat_train_set, strat_test_set):
                set_.drop("income_cat", axis=1, inplace=True)

            # store train and test data
            strat_train_set.to_csv (dataset_path + "/housing_train.csv")
            strat_test_set.to_csv (dataset_path + "/housing_test.csv")
            self.send_metrics("splitting_data", 100)

            # explore and visualize
            # histogram
            self.send_metrics("exploring_data", 0)
            housing.hist(bins=50, figsize=(20, 15))
            self.save_fig(fig_id="attribute_histogram_plots", images_path=images_path)
            self.send_metrics("exploring_data", 50)

            # scatter plot
            housing.plot(kind="scatter", x="longitude", y="latitude", alpha=0.4,
                         s=housing["population"] / 100, label="population", figsize=(10, 7),
                         c="median_house_value", cmap=plt.get_cmap("jet"), colorbar=True,
                         sharex=False)
            plt.legend()
            self.save_fig(fig_id="housing_prices_scatterplot", images_path=images_path)
            self.send_metrics("exploring_data", 100)

        self.completed()

    def send_metrics(self, metric_name, percent_completed):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_exploration"},
            "metric": {metric_name: percent_completed}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=False)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = ExploreData()
    if len(sys.argv) >= 4:
        data_prep.start(run_name=sys.argv[1], dataset_path=sys.argv[2], images_path=sys.argv[3])
    else:
        data_prep.start(run_name="")
