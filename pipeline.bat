set PYTHONPATH=%PYTHONPATH%;d:\abzooba\workspace\demo_housing_price_predictor\xpresso_ai
set XPRESSO_PACKAGE_PATH=d:\abzooba\workspace\demo_housing_price_predictor\xpresso_ai
set XPRESSO_CONFIG_PATH=config/common.json
set enable_local_execution=True
cd fetch_data
..\venv\scripts\python app\app.py dummy_run https://raw.githubusercontent.com/ageron/handson-ml2/master/datasets/housing/housing.tgz /data/datasets
cd ..\explore_data
..\venv\scripts\python app\app.py dummy_run /data/datasets /data/images
cd ..\prepare_data
..\venv\scripts\python app\app.py dummy_run /data/datasets
cd ..\train_model
..\venv\scripts\python app\app.py dummy_run /data/datasets

